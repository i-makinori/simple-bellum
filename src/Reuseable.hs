module Reuseable where

nth :: Int -> [a] -> Maybe a
nth _ [] = Nothing
nth n list@(x:xs)
  | n < 0 = Nothing
  | n == 0 = Just x
  | otherwise = nth ((-) n 1) xs

show_matrix :: Show a => [a] -> String
show_matrix = foldr1 (flip (++).(++) "\n") . map show . reverse

maybe_list_to_list :: Maybe [a] -> [a]
maybe_list_to_list Nothing   = []
maybe_list_to_list (Just x)  = x


