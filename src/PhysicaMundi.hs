
module PhysicaMundi where

import Reuseable


-- agriager

-- estructura de spatium

-- cordinate system

data Coordinate = Coordinate Int Int
  deriving (Show, Eq)

coordX :: Coordinate -> Int
coordX (Coordinate x _) = x

coordY :: Coordinate -> Int
coordY (Coordinate _ y) = y



-- terrain in mundo


data AgriTile = Area | Wall | Nil
  deriving(Show, Eq)

type AgriAger = [[AgriTile]]

-- type Height = Int
-- type Width = Int

-- phisica

minmin_maxmax :: Coordinate -> Coordinate -> (Coordinate, Coordinate)
minmin_maxmax (Coordinate c1_x c1_y) (Coordinate c2_x c2_y)
  = (Coordinate (min c1_x c2_x) (min c1_y c2_y),
     Coordinate (max c1_x c2_x) (max c1_y c2_y))

coord_tile :: Coordinate -> AgriAger -> AgriTile
coord_tile coord@(Coordinate x y) agriager
  = case nth x =<< nth y agriager of
      Nothing -> Nil
      Just a  -> a

agri_take :: Int -> [AgriTile] -> [AgriTile]
agri_take 0 _ = []
agri_take n [] = Nil : agri_take ((-) n 1) []
agri_take n (x:xs) = x : agri_take ((-) n 1) xs

agri_drop :: Int -> [AgriTile] -> [AgriTile]
agri_drop 0 list = list
agri_drop n list
  | n < 0 = Nil : agri_drop ((+) n 1) list
  | n > 0 = agri_drop ((-) n 1) (tail list)
  | otherwise = error "agridrop something wrong"

agri_range_row :: Int -> Int -> [AgriTile] -> [AgriTile] -> [AgriTile]
agri_range_row n_min n_max current list
  | n_max+1 == n_min = reverse current
  | n_min > n_max = error "min > max"
  | null list = agri_range_row (n_min+1) (n_max+0) (Nil:current) []
  | n_min ==0 = agri_range_row (n_min-0) (n_max-1) (head list:current) (tail list)
  | n_min < 0 = agri_range_row (n_min+1) (n_max+0) (Nil:current) list
  | n_min > 0 = agri_range_row (n_min-1) (n_max-1) current (tail list)

agri_range :: Int -> Int -> [AgriTile] -> [AgriTile]
agri_range n1 n2 list  = (if n1>n2 then reverse else id) range_row
  where range_row = agri_range_row (min n1 n2) (max n1 n2) [] list

agriager_range :: Coordinate -> Coordinate -> AgriAger -> AgriAger
agriager_range c1@(Coordinate c1_x c1_y) c2@(Coordinate c2_x c2_y) agriager
  = flip map y_range
    (\y -> agri_range c1_x c2_x $ maybe_list_to_list $ nth y agriager)
  where min_x = min (coordX c1) (coordX c2)
        max_x = max (coordX c1) (coordX c2)
        y_range = count_list c1_y c2_y
        count_list n1 n2 = (if n1>n2 then reverse else id) [min n1 n2 .. max n1 n2]


-- conversion

tile_char_mensam :: ((AgriTile,Char), [(AgriTile, Char)])
tile_char_mensam =
  ((Nil,'E'), -- otherwise
    [(Wall, '#'), -- by case
     (Area, '.'),
     (Nil , ' ')])

agriager_to_str_list :: AgriAger -> [String]
agriager_to_str_list tiles_list = (map.map) tile_to_char tiles_list
  where tile_to_char :: AgriTile -> Char
        tile_to_char tile
          = foldl (\cur tup@(t, c) -> if t==tile then c else cur)
            ((snd.fst) tile_char_mensam) (snd tile_char_mensam)

str_list_to_agriager :: [String] -> AgriAger
str_list_to_agriager str_list = (map.map) char_to_tile str_list
  where char_to_tile :: Char -> AgriTile
        char_to_tile char
          = foldl (\cur tup@(t, c) -> if c==char then t else cur)
            ((fst.fst) tile_char_mensam) (snd tile_char_mensam)


-- test
-- Agri example
test_agri_str_list =
  [ "####################################" ,
    "#...#..............................#" ,
    "#aa.#..............................#" , -- 'a' => Nil
    "#...#..............................#" ,
    "#..................................#" ,
    "#..................................#" ,
    "#..................................#" ,
    "#..................................#" ,
    "#..................................#" ,
    "#..................................#" ,
    "#..................................#" ,
    "#..................................#" ,
    "#..................................#" ,
    "#..................................#" ,
    "####################################"
  ]


test_agriager = str_list_to_agriager test_agri_str_list


test_agriager_range =
  agriager_range (Coordinate (10) (3)) (Coordinate (-5) (-11)) test_agriager

