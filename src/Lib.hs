
module Lib where

import PhysicaMundi
import Reuseable



-- rulebase

type Coetus = Char
type CreaturaID = Int


-- MomentaneumPerception

data Fog = Liquet | Know | Incognita deriving (Show, Eq)

data VideturElementia =
  VideElement Fog (Maybe AgriTile) [Creatura]
  deriving (Show, Eq)

type CreaturaVison = [[VideturElementia]]

data MomentaneumPerception = MomentPerceptio
  { creatura_id_perception :: CreaturaID,
    visions :: CreaturaVison }
  deriving (Show)


-- Motus
data DeltaMotus = Decrement | Sine | Increment
  deriving (Show, Eq)

deltaMotus_value :: DeltaMotus -> Int
deltaMotus_value Decrement = (0-1)
deltaMotus_value Increment = (0+1)
deltaMotus_value _ = 0

data MotusDirection = MotusDirection DeltaMotus DeltaMotus
  deriving (Show, Eq)

directionX :: MotusDirection -> Int
directionX (MotusDirection x _) = deltaMotus_value x

directionY :: MotusDirection -> Int
directionY (MotusDirection _ y) =  deltaMotus_value y


-- MomentaneumReactionem

data MomentaneumReactionem = MomentReactionem
  { creatura_id_reactionem :: CreaturaID,
    motus_direction :: MotusDirection,
    corporis_direction :: MotusDirection}
  deriving (Show)

-- creatura

data Creatura = Creatura
  { creatura_id_creatura :: CreaturaID,
    team :: Coetus,
    vis_oculos :: Int,
    coord :: Coordinate,
    direction :: MotusDirection,
    obside :: Bool -- or dead
  }
  deriving (Show, Eq)


-- law

-- [creatura] >>= [want_to_go] >>= [creatura]
-- [(current,team,id)], [(want,id)] -> [(next, id)]


aut_bounce_off_inimicos :: Coordinate -> [Coordinate] -> Bool
aut_bounce_off_inimicos eius_coord inimicos_coords = undefined
  -- XX : elem eius_coord inimicos_coords

aut_bounce_off_not_area :: Coordinate -> AgriAger -> Bool
aut_bounce_off_not_area eius_coord agriager
  = case coord_tile eius_coord agriager of
      Area -> False
      otherwise -> True

motus_in_agri :: Coordinate -> MotusDirection -> AgriAger -> Coordinate
motus_in_agri current_coord motus_dir agri = undefined
  where
    directio_intentionis = Coordinate
                           (directionX motus_dir + coordX current_coord)
                           (directionY motus_dir + coordY current_coord)



data DirectionGenus = DirectionVertex | DirectionCentrum | DirectionCrux deriving (Show, Eq)

directionGenus :: MotusDirection -> DirectionGenus
directionGenus (MotusDirection x y)
  | x/=Sine && y/=Sine = DirectionVertex
  | x==Sine && y==Sine = DirectionCentrum
  | (x==Sine && y/=Sine) || (x/=Sine && y==Sine) = DirectionCrux
  | otherwise = error "directionGenus something wrong"

aut_damnum_direction :: MotusDirection -> MotusDirection -> Bool
aut_damnum_direction origin target
  | direction_genus == DirectionCentrum = True
  | direction_genus == DirectionVertex && dx+dy > 1 = True
  | direction_genus == DirectionCrux && dx+dy > 1 = True
  | otherwise = False
  where
    dx = abs(directionX origin - directionY target)
    dy = abs(directionY origin - directionY target)
    direction_genus = directionGenus target


-- bouce and (self.coord is enemy) => dead
-- (damage_direction_p (some emeny.coord==self.coord).direction)==t => dead


dein_creatures :: [Creatura] -> [MomentaneumReactionem] -> [AgriAger] -> [Creatura]
dein_creatures c m a = undefined


-- law nam singula creatura


-- law nam Coetus collectione






-- Game

data Game = Game
  { agriager:: AgriAger,
    creaturas :: [Creatura]}


